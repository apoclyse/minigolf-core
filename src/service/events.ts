import { pool } from "./mysql";
import { EventDb } from "../model/event/event-db";
import { EventDto } from "../model/event/event-dto";
import { EventCreateUpdate } from "../model/event/event-create-update";
import { Error } from "../model/error/error";
import { EventStateUpdate } from "../model/event/event-state-update";
import { EventYearsDb } from "../model/event/event-years-db";

function getAllEvents(): Promise<EventDto[]> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        "SELECT * FROM events",
        (err: any, eventDbs: EventDb[]) => {
          connection.release();
          if (err) return reject(err);
          const eventDtos: EventDto[] = eventDbs.map((eventDb) =>
            EventDto.fromDb(eventDb)
          );
          return resolve(eventDtos);
        }
      );
    });
  });
}

function getEventsInYear(year: string = ""): Promise<EventDto[]> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function(e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "SELECT * FROM events WHERE dateFrom LIKE ?",
          values: [`%${year}`]
        },
        (err: any, eventDbs: EventDb[]) => {
          connection.release();
          if (err) return reject(err);
          const eventDtos: EventDto[] = eventDbs.map((eventDb) =>
            EventDto.fromDb(eventDb)
          );
          return resolve(eventDtos);
        }
      );
    });
  });

}


function getAvailableYears(): Promise<string[]> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function(e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        "SELECT dateFrom FROM events",
        (err: any, years: EventYearsDb[]) => {
          connection.release();
          if (err) return reject(err);
          const result: string[] = [...new Set(years.map((y) => y.dateFrom.split(".")[2]))];
          return resolve(result);
        }
      );
    });
  });

}

function getEvent(id: string): Promise<EventDto> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "SELECT * FROM events WHERE id=? LIMIT 0, 1",
          values: [id],
        },
        (err: any, eventDbs: EventDb[]) => {
          connection.release();
          if (err) return reject(err);
          const eventDb: EventDb = eventDbs[0];
          if (!eventDb) {
            return reject(new Error(404, "event not found by id"));
          }
          const eventDto: EventDto = EventDto.fromDb(eventDb);
          return resolve(eventDto);
        }
      );
    });
  });
}

function createEvent(eventCreateUpdate: EventCreateUpdate): Promise<any> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "INSERT INTO events (name, organizer, dateFrom, dateTo, created) VALUES (?, ?, ?, ?, CURDATE())",
          values: [
            eventCreateUpdate.name,
            eventCreateUpdate.organizer,
            eventCreateUpdate.dateFrom,
            eventCreateUpdate.dateTo,
          ],
        },
        (err: any, res: any) => {
          connection.release();
          if (err) reject(err);
          resolve(res);
        }
      );
    });
  });
}

function updateEvent(
  id: string,
  eventCreateUpdate: EventCreateUpdate
): Promise<any> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "UPDATE events SET name=?, organizer=?, dateFrom=?, dateTo=? WHERE id=?",
          values: [
            eventCreateUpdate.name,
            eventCreateUpdate.organizer,
            eventCreateUpdate.dateFrom,
            eventCreateUpdate.dateTo,
            id,
          ],
        },
        (err: any, res: any) => {
          connection.release();
          if (err) return reject(err);
          return resolve(res);
        }
      );
    });
  });
}

function updateEventState(
  id: string,
  eventStateUpdate: EventStateUpdate
): Promise<any> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "UPDATE events SET state=? WHERE id=?",
          values: [eventStateUpdate.state, id],
        },
        (err: any, res: any) => {
          connection.release();
          if (err) return reject(err);
          return resolve(res);
        }
      );
    });
  });
}

function deleteEvent(id: number): Promise<any> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "DELETE FROM events WHERE id=?; DELETE FROM rounds WHERE event_id=?; DELETE FROM penalty WHERE event_id=?; DELETE FROM scores WHERE event_id=?; DELETE FROM teams WHERE event_id=?; DELETE FROM snapshots WHERE event_id=?",
          values: [id, id, id, id, id, id],
        },
        (err: any, res: any) => {
          connection.release();
          if (err) return reject(err);
          return resolve(res);
        }
      );
    });
  });
}

export {
  getAllEvents,
  getEvent,
  createEvent,
  updateEvent,
  updateEventState,
  deleteEvent,
  getEventsInYear,
  getAvailableYears
};
