import { pool } from "./mysql";
import { SnapshotDto } from "../model/snapshot/snapshot-dto";
import { SnapshotCreate } from "../model/snapshot/snapshot-create";
import { SnapshotDb } from "../model/snapshot/snapshot-db";
import { Error } from "../model/error/error";

function getSnapshot(eventId: number): Promise<SnapshotDto> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "SELECT * FROM snapshots WHERE event_id=? LIMIT 0, 1",
          values: [eventId],
        },
        (err: any, snapshotDbs: SnapshotDb[]) => {
          connection.release();
          if (err) return reject(err);
          const snapshotDb: SnapshotDb = snapshotDbs[0];
          if (!snapshotDb)
            return reject(new Error(404, "snapshot not found by event id"));
          return resolve(SnapshotDto.fromDb(snapshotDb));
        }
      );
    });
  });
}

function createSnapshot(snapshotCreate: SnapshotCreate): Promise<any> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "INSERT INTO snapshots (created, event_id, snapshot) VALUES (CURDATE(), ?, ?) ON DUPLICATE KEY UPDATE snapshot=?",
          values: [
            snapshotCreate.eventId,
            snapshotCreate.snapshot,
            snapshotCreate.snapshot,
          ],
        },
        (err: any, res: any) => {
          connection.release();
          if (err) return reject(err);
          return resolve(res);
        }
      );
    });
  });
}

function deleteSnapshot(eventId: string): Promise<any> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "DELETE FROM snapshots WHERE event_id=?",
          values: [eventId],
        },
        (err: any, res: any) => {
          connection.release();
          if (err) return reject(err);
          return resolve(res);
        }
      );
    });
  });
}

export { getSnapshot, createSnapshot, deleteSnapshot };
