import { pool } from "./mysql";
import { ScoreDb } from "../model/score/score-db";
import { ScoreDto } from "../model/score/score-dto";
import { ScoreCreate } from "../model/score/score-create";
import { ScoreUpdate } from "../model/score/score-update";
import { ScoreCategoryUpdate } from "../model/score/score-category-update";
import { SCORE } from "../model/score/SCORE";
import { Error } from "../model/error/error";
import { getLoginById } from "./login";
import { Role } from "../model/login/role";

function getScores(eventId: number): Promise<ScoreDto[]> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        { sql: "SELECT * FROM scores WHERE event_id=?", values: [eventId] },
        (err: any, scoreDbs: ScoreDb[]) => {
          connection.release();
          if (err) return reject(err);
          return resolve(scoreDbs.map((scoreDb) => ScoreDto.fromDb(scoreDb)));
        }
      );
    });
  });
}

function getPlayerIds(eventId: number): Promise<number[]> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "SELECT player_id FROM scores WHERE event_id=?",
          values: [eventId],
        },
        (err: any, playerIds: { player_id: number }[]) => {
          connection.release();
          if (err) return reject(err);
          return resolve(playerIds.map((v) => v.player_id));
        }
      );
    });
  });
}

function getScore(id: number): Promise<ScoreDto> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        { sql: "SELECT * FROM scores WHERE id=? LIMIT 0, 1", values: [id] },
        (err: any, scoreDbs: ScoreDb[]) => {
          connection.release();
          if (err) return reject(err);
          const scoreDb: ScoreDb = scoreDbs[0];
          if (!scoreDb) return reject(new Error(404, "score not found by id"));
          return resolve(ScoreDto.fromDb(scoreDb));
        }
      );
    });
  });
}

function createScore(scoreCreate: ScoreCreate): Promise<any> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "INSERT INTO scores (created, event_id, player_id, club_id, score, player_category) VALUES (CURDATE(), ?, ?, ?, ?, ?)",
          values: [
            scoreCreate.eventId,
            scoreCreate.playerId,
            scoreCreate.clubId,
            SCORE.stringify([]),
            scoreCreate.playerCategory,
          ],
        },
        (err: any, res: any) => {
          connection.release();
          if (err) return reject(err);
          return resolve(res);
        }
      );
    });
  });
}

function updateScore(id: string, scoreUpdate: ScoreUpdate): Promise<any> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "UPDATE scores SET score=? WHERE id=?",
          values: [SCORE.stringify(scoreUpdate.score), id],
        },
        (err: any, res: any) => {
          connection.release();
          if (err) return reject(err);
          return resolve(res);
        }
      );
    });
  });
}

function updateScoreCategory(id: string, scoreCategoryUpdate: ScoreCategoryUpdate): Promise<any> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }
      connection.query(
        {
          sql: "UPDATE scores set player_category=? WHERE id=?",
          values: [scoreCategoryUpdate.category, id],
        },
        (err: any, res: any) => {
          connection.release();
          if (err) return reject(err);
          return resolve(res);
        }
      );
    });
  });
}

function deleteScore(id: string): Promise<any> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "DELETE FROM scores WHERE id=?; DELETE FROM penalty WHERE score_id=?",
          values: [id, id],
        },
        (err: any, res: any) => {
          connection.release();
          if (err) return reject(err);
          return resolve(res);
        }
      );
    });
  });
}

function scoresNotExist(eventId: number): Promise<void> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        { sql: "SELECT * FROM scores WHERE event_id=?", values: [eventId] },
        (err: any, scoreDbs: ScoreDb[]) => {
          connection.release();
          if (err) return reject(err);
          return scoreDbs.length > 0
            ? reject("scores exist on this event")
            : resolve();
        }
      );
    });
  });
}

function scoresAuth(eventId: number, req: any, res: any, next: any): any {
  getLoginById(req.body.decodedLogin.id).then(
    (loginDto) => {
      if (loginDto.role === Role.ADMIN || loginDto.role === Role.OWNER)
        return next();
      else if (!loginDto.eventId)
        return res.status(403).send("forbidden operation");
      else if (eventId !== loginDto.eventId)
        return res.status(403).send("forbidden operation");
      return next();
    },
    (err) => {
      return res.status(403).send(err);
    }
  );
}

function scoreAuth(scoreId: string, req: any, res: any, next: any): any {
  getLoginById(req.body.decodedLogin.id).then(
    (loginDto) => {
      if (loginDto.role === Role.ADMIN || loginDto.role === Role.OWNER) {
        return next();
      }

      if (!loginDto.eventId) {
        return res.status(403).send("forbidden operation");
      }

      pool.getConnection(function (e: any, connection: any) {
        if (e) {
          connection.release();
          return res.status(403).send("forbidden operation");
        }

        connection.query(
          {
            sql: "SELECT * FROM scores WHERE id=? LIMIT 0, 1",
            values: [scoreId],
          },
          (err: any, scoreDbs: ScoreDb[]) => {
            connection.release();
            if (err) return res.status(403).send("forbidden operation");
            else if (
              scoreDbs.length === 0 ||
              scoreDbs[0].event_id !== loginDto.eventId
            ) {
              return res.status(403).send("forbidden operation");
            }
            return next();
          }
        );
      });
    },
    (err) => {
      return res.status(403).send(err);
    }
  );
}

export {
  getScores,
  getPlayerIds,
  getScore,
  createScore,
  updateScore,
  updateScoreCategory,
  deleteScore,
  scoresNotExist,
  scoresAuth,
  scoreAuth,
};
