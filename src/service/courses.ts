import { pool } from "./mysql";
import { CourseDb } from "../model/course/course-db";
import { CourseDto } from "../model/course/course-dto";
import { CourseCreateUpdate } from "../model/course/course-create-update";
import { Exist } from "../model/error/exist";
import { Table } from "../constant/table";
import { RoundDb } from "../model/round/round-db";
import { RoundDto } from "../model/round/round-dto";
import { Error } from "../model/error/error";

function getAllCourses(): Promise<CourseDto[]> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        "SELECT * FROM courses",
        (err: any, courseDbs: CourseDb[]) => {
          connection.release();
          if (err) return reject(err);
          const courseDtos: CourseDto[] = courseDbs.map((courseDb) =>
            CourseDto.fromDb(courseDb)
          );
          return resolve(courseDtos);
        }
      );
    });
  });
}

function getCourse(id: string): Promise<CourseDto> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        { sql: "SELECT * FROM courses WHERE id=? LIMIT 0, 1", values: [id] },
        (err: any, courseDbs: CourseDb[]) => {
          connection.release();
          if (err) return reject(err);
          const courseDb: CourseDb = courseDbs[0];
          if (courseDb) {
            const courseDto: CourseDto = CourseDto.fromDb(courseDb);
            return resolve(courseDto);
          }
          return reject(new Error(404, "course not found by id"));
        }
      );
    });
  });
}

function createCourse(courseCreateUpdate: CourseCreateUpdate): Promise<any> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "INSERT INTO courses (name, created, lane_1, lane_2, lane_3, lane_4, lane_5, lane_6, lane_7, lane_8, lane_9, lane_10, lane_11, lane_12, lane_13, lane_14, lane_15, lane_16, lane_17, lane_18) VALUES (?, CURDATE(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
          values: [
            courseCreateUpdate.name,
            courseCreateUpdate.lane1,
            courseCreateUpdate.lane2,
            courseCreateUpdate.lane3,
            courseCreateUpdate.lane4,
            courseCreateUpdate.lane5,
            courseCreateUpdate.lane6,
            courseCreateUpdate.lane7,
            courseCreateUpdate.lane8,
            courseCreateUpdate.lane9,
            courseCreateUpdate.lane10,
            courseCreateUpdate.lane11,
            courseCreateUpdate.lane12,
            courseCreateUpdate.lane13,
            courseCreateUpdate.lane14,
            courseCreateUpdate.lane15,
            courseCreateUpdate.lane16,
            courseCreateUpdate.lane17,
            courseCreateUpdate.lane18,
          ],
        },
        (err: any, res: any) => {
          connection.release();
          if (err) return reject(err);
          return resolve(res);
        }
      );
    });
  });
}

function updateCourse(
  id: string,
  courseCreateUpdate: CourseCreateUpdate
): Promise<any> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "UPDATE courses SET name=?, lane_1=?, lane_2=?, lane_3=?, lane_4=?, lane_5=?, lane_6=?, lane_7=?, lane_8=?, lane_9=?, lane_10=?, lane_11=?, lane_12=?, lane_13=?, lane_14=?, lane_15=?, lane_16=?, lane_17=?, lane_18=? WHERE id=?",
          values: [
            courseCreateUpdate.name,
            courseCreateUpdate.lane1,
            courseCreateUpdate.lane2,
            courseCreateUpdate.lane3,
            courseCreateUpdate.lane4,
            courseCreateUpdate.lane5,
            courseCreateUpdate.lane6,
            courseCreateUpdate.lane7,
            courseCreateUpdate.lane8,
            courseCreateUpdate.lane9,
            courseCreateUpdate.lane10,
            courseCreateUpdate.lane11,
            courseCreateUpdate.lane12,
            courseCreateUpdate.lane13,
            courseCreateUpdate.lane14,
            courseCreateUpdate.lane15,
            courseCreateUpdate.lane16,
            courseCreateUpdate.lane17,
            courseCreateUpdate.lane18,
            id,
          ],
        },
        (err: any, res: any) => {
          connection.release();
          if (err) return reject(err);
          return resolve(res);
        }
      );
    });
  });
}

function deleteCourse(id: string): Promise<any> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "DELETE FROM courses WHERE id=?",
          values: [id],
        },
        (err: any, res: any) => {
          connection.release();
          if (err) return reject(err);
          return resolve(res);
        }
      );
    });
  });
}

function courseInUse(id: string): Promise<Exist[]> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (e: any, connection: any) {
      if (e) {
        connection.release();
        return reject(e);
      }

      connection.query(
        {
          sql: "SELECT * FROM rounds",
        },
        (err: any, roundDbs: RoundDb[]) => {
          connection.release();
          if (err) return reject(err);
          const exist: Exist[] = [];

          roundDbs.forEach((roundDb) => {
            const roundDto: RoundDto = RoundDto.fromDb(roundDb);
            if (
              roundDto.rounds.some((round) => round.courseId === Number(id))
            ) {
              exist.push(new Exist(roundDto.id, Table.ROUNDS));
            }
          });

          return exist.length > 0 ? reject(exist) : resolve(exist);
        }
      );
    });
  });
}

export {
  getAllCourses,
  getCourse,
  createCourse,
  updateCourse,
  deleteCourse,
  courseInUse,
};
