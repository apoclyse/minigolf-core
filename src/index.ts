import express, { Application } from "express";
import bodyParser from "body-parser";
import { connectToMysql } from "./service/mysql";
import events from "./router/events";
import players from "./router/players";
import courses from "./router/courses";
import rounds from "./router/rounds";
import scores from "./router/scores";
import penalty from "./router/penalty";
import teams from "./router/teams";
import cups from "./router/cups";
import clubs from "./router/clubs";
import login from "./router/login";
import snapshot from "./router/snapshot";

const app: Application = express();
const PORT = process.env.PORT || 8000;

var cors = require("cors");

// MySql connection
connectToMysql();

//Body Parser MW
app.use(bodyParser.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));

//odomkne porty
app.use(cors({ allowedOrigins: "*" }));

//API
app.use("/api", events);
app.use("/api", players);
app.use("/api", courses);
app.use("/api", rounds);
app.use("/api", scores);
app.use("/api", penalty);
app.use("/api", teams);
app.use("/api", cups);
app.use("/api", clubs);
app.use("/api", login);
app.use("/api", snapshot);

app.listen(PORT, (): void => {
  console.log(`Server listening on port ${PORT}`);
});
