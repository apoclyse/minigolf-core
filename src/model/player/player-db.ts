export interface PlayerDb {
  id: number;
  created: string;
  firstname: string;
  lastname: string;
  category: string;
  club_id: number;
}
