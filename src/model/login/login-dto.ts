import { LoginDb } from "./login-db";
import { Role } from "./role";

export class LoginDto {
  id: number;
  created: string;
  login: string;
  role: Role;
  token: string;
  eventId: number;

  constructor(
    id: number,
    created: string,
    login: string,
    role: Role,
    token: string,
    eventId: number
  ) {
    this.id = id;
    this.created = created;
    this.login = login;
    this.role = role;
    this.token = token;
    this.eventId = eventId;
  }

  static fromDb(loginDb: LoginDb, token: string): LoginDto {
    return new LoginDto(
      loginDb.id,
      loginDb.created,
      loginDb.login,
      loginDb.role,
      token,
      loginDb.event_id
    );
  }
}
