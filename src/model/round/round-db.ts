export interface RoundDb {
  id: number;
  created: string;
  event_id: number;
  rounds: string;
}
