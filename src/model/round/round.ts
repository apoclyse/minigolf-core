export interface Round {
  courseId: number;
  name: string;
  includeInTeam: boolean;
}
