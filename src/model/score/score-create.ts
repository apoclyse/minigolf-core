export interface ScoreCreate {
  eventId: number;
  playerId: number;
  clubId: number;
  playerCategory: string;
}
