import { ScoreDb } from "./score-db";
import { SCORE } from "./SCORE";

export class ScoreDto {
  id: number;
  created: string;
  eventId: number;
  playerId: number;
  clubId: number;
  score: number[][];
  playerCategory: string;

  constructor(
    id: number,
    created: string,
    eventId: number,
    playerId: number,
    clubId: number,
    score: number[][],
    playerCategory: string
  ) {
    this.id = id;
    this.created = created;
    this.eventId = eventId;
    this.playerId = playerId;
    this.clubId = clubId;
    this.score = score;
    this.playerCategory = playerCategory;
  }

  static fromDb(scoreDb: ScoreDb): ScoreDto {
    return new ScoreDto(
      scoreDb.id,
      scoreDb.created,
      scoreDb.event_id,
      scoreDb.player_id,
      scoreDb.club_id,
      SCORE.parse(scoreDb.score),
      scoreDb.player_category
    );
  }
}
