import { PenaltyDb } from "./penalty-db";
import { PenaltyType } from "./penalty-type";

export class PenaltyDto {
  id: number;
  created: string;
  eventId: number;
  roundIndex: number;
  scoreId: number;
  playerId: number;
  penalty: number;
  type: PenaltyType;
  text: string;

  constructor(
    id: number,
    created: string,
    eventId: number,
    roundIndex: number,
    scoreId: number,
    playerId: number,
    penalty: number,
    type: PenaltyType,
    text: string
  ) {
    this.id = id;
    this.created = created;
    this.eventId = eventId;
    this.roundIndex = roundIndex;
    this.scoreId = scoreId;
    this.playerId = playerId;
    this.penalty = penalty;
    this.type = type;
    this.text = text;
  }

  static fromDb(penaltyDb: PenaltyDb): PenaltyDto {
    return new PenaltyDto(
      penaltyDb.id,
      penaltyDb.created,
      penaltyDb.event_id,
      penaltyDb.round_index,
      penaltyDb.score_id,
      penaltyDb.player_id,
      penaltyDb.penalty,
      penaltyDb.type as PenaltyType,
      penaltyDb.text
    );
  }
}
