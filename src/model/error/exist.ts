import { Table } from "../../constant/table";

export class Exist {
  id: number;
  table: Table;

  constructor(id: number, table: Table) {
    this.id = id;
    this.table = table;
  }
}
