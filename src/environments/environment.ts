export const environment = {
  production: true,
  tokenKey: "minigolf-core",
  tokenExpiresIn: "1d",

  dbDev: {
    host: "localhost",
    user: "root",
    password: "passworD1.",
    database: "minigolf",
  },
  dbProd: {
    host: "localhost",
    user: "root",
    password: "passworD1.",
    database: "minigolf",
  }
};
