export const scoreSum = (scores: number[]): number =>
  scores.reduce((previousValue, currentValue) => previousValue + currentValue);
