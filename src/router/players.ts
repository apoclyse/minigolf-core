import express, { Request, Response, Router } from "express";
import { PlayerCreateUpdate } from "../model/player/player-create-update";
import {
  createPlayer,
  deletePlayer,
  getAllPlayers,
  getPlayer,
  getPlayersByEventId,
  playerInUse,
  updatePlayer,
} from "../service/players";
import auth from "../service/auth";
import { Role } from "../model/login/role";

const router: Router = express.Router();

router.get("/players", (req: Request, res: Response): void => {
  getAllPlayers().then(
    (playerDtos) => {
      res.json(playerDtos);
    },
    (err) => {
      res.status(500).send(err);
    }
  );
});

router.get(
  "/players/:eventId",
  (req: { params: { eventId: string } }, res: Response): void => {
    getPlayersByEventId(Number(req.params.eventId)).then(
      (playerDtos) => {
        res.json(playerDtos);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

router.get(
  "/player/:id",
  (req: { params: { id: string } }, res: Response): void => {
    getPlayer(req.params.id).then(
      (playerDto) => {
        res.json(playerDto);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

router.post(
  "/players",
  auth,
  (req: { body: PlayerCreateUpdate }, res: Response): void => {
    createPlayer(req.body).then(
      (_) => {
        res.json(_);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

router.put(
  "/player/:id",
  auth.bind([Role.ADMIN, Role.OWNER]),
  (
    req: { params: { id: string }; body: PlayerCreateUpdate },
    res: Response
  ): void => {
    updatePlayer(Number(req.params.id), req.body).then(
      (_) => {
        res.json(_);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

router.delete(
  "/player/:id",
  auth.bind([Role.ADMIN, Role.OWNER]),
  (req: { params: { id: string } }, res: Response): void => {
    playerInUse(Number(req.params.id))
      .then(() =>
        deletePlayer(Number(req.params.id)).then(
          (_) => res.json(_),
          (err) => res.status(500).send(err)
        )
      )
      .catch((err) => res.status(403).send(err));
  }
);

export default router;
