import express, { Request, Response, Router } from "express";
import {
  createEvent,
  deleteEvent,
  getAllEvents,
  getEvent,
  updateEvent,
  updateEventState,
  getEventsInYear,
  getAvailableYears
} from "../service/events";
import { EventCreateUpdate } from "../model/event/event-create-update";
import auth from "../service/auth";
import { scoresNotExist } from "../service/scores";
import { EventStateUpdate } from "../model/event/event-state-update";
import { Role } from "../model/login/role";

const router: Router = express.Router();

router.get("/events", (req: Request, res: Response): void => {
  let year = req.query?.year as string;
  if (!year) {
    getAllEvents().then(
      (eventDtos) => {
        res.json(eventDtos);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  } else {
    getEventsInYear(year).then(
      (eventDtos) => {
        res.json(eventDtos);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }

});

router.get("/events/years", (req: Request, res: Response): void => {
  getAvailableYears().then(
    (years) => {
      res.json(years);
    },
    (err) => {
      res.status(500).send(err);
    }
  );
});

router.get(
  "/event/:id",
  (req: { params: { id: string } }, res: Response): void => {
    getEvent(req.params.id).then(
      (eventDto) => {
        res.json(eventDto);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

router.post(
  "/events",
  auth.bind([Role.ADMIN, Role.OWNER]),
  (req: { body: EventCreateUpdate }, res: Response): void => {
    createEvent(req.body).then(
      (_) => {
        res.json(_);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

router.put(
  "/event/:id",
  auth.bind([Role.ADMIN, Role.OWNER]),
  (
    req: { params: { id: string }; body: EventCreateUpdate },
    res: Response
  ): void => {
    updateEvent(req.params.id, req.body).then(
      (_) => {
        res.json(_);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

router.put(
  "/event/:id/state",
  auth.bind([Role.ADMIN, Role.OWNER]),
  (
    req: { params: { id: string }; body: EventStateUpdate },
    res: Response
  ): void => {
    updateEventState(req.params.id, req.body).then(
      (_) => {
        res.json(_);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

router.delete(
  "/event/:id",
  auth.bind([Role.ADMIN, Role.OWNER]),
  (req: { params: { id: string } }, res: Response): void => {
    scoresNotExist(Number(req.params.id))
      .then(() =>
        deleteEvent(Number(req.params.id)).then(
          (_) => res.json(_),
          (err) => res.status(500).send(err)
        )
      )
      .catch((err) => res.status(403).send(err));
  }
);

export default router;
