import express, { Request, Response, Router } from "express";
import { CourseCreateUpdate } from "../model/course/course-create-update";
import {
  courseInUse,
  createCourse,
  deleteCourse,
  getAllCourses,
  getCourse,
  updateCourse,
} from "../service/courses";
import auth from "../service/auth";
import { Role } from "../model/login/role";

const router: Router = express.Router();

router.get("/courses", (req: Request, res: Response): void => {
  getAllCourses().then(
    (courseDtos) => {
      res.json(courseDtos);
    },
    (err) => {
      res.status(500).send(err);
    }
  );
});

router.get(
  "/course/:id",
  (req: { params: { id: string } }, res: Response): void => {
    getCourse(req.params.id).then(
      (courseDto) => {
        res.json(courseDto);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

router.post(
  "/courses",
  auth,
  (req: { body: CourseCreateUpdate }, res: Response): void => {
    createCourse(req.body).then(
      (_) => {
        res.json(_);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

router.put(
  "/course/:id",
  auth.bind([Role.ADMIN, Role.OWNER]),
  (
    req: { params: { id: string }; body: CourseCreateUpdate },
    res: Response
  ): void => {
    updateCourse(req.params.id, req.body).then(
      (_) => {
        res.json(_);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

router.delete(
  "/course/:id",
  auth.bind([Role.ADMIN, Role.OWNER]),
  (req: { params: { id: string } }, res: Response): void => {
    courseInUse(req.params.id)
      .then(() =>
        deleteCourse(req.params.id).then(
          (_) => res.json(_),
          (err) => res.status(500).send(err)
        )
      )
      .catch((err) => res.status(403).send(err));
  }
);

export default router;
