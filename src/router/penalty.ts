import express, { Response, Router } from "express";
import { PenaltyCreateUpdate } from "../model/penalty/penalty-create-update";
import {
  createPenalty,
  deleteAllPenaltyByEvent,
  deletePenalty,
  getAllPenalty,
  getPenalty,
  penaltiesAuth,
  penaltyAuth,
  updatePenalty,
} from "../service/penalty";
import auth from "../service/auth";

const router: Router = express.Router();

router.get(
  "/penalty/:eventId",
  (req: { params: { eventId: string } }, res: Response): void => {
    getAllPenalty(Number(req.params.eventId)).then(
      (penaltyDtos) => {
        res.json(penaltyDtos);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

router.get(
  "/penalty/:scoreId/score",
  (req: { params: { scoreId: string } }, res: Response): void => {
    getPenalty(Number(req.params.scoreId)).then(
      (penaltyDtos) => {
        res.json(penaltyDtos);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

router.post(
  "/penalty",
  auth,
  (req, res, next) => penaltiesAuth(req.body.eventId, req, res, next),
  (req: { body: PenaltyCreateUpdate }, res: Response): void => {
    createPenalty(req.body).then(
      (_) => {
        res.json(_);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

router.put(
  "/penalty/:id",
  auth,
  (req, res, next) => penaltyAuth(req.params.id, req, res, next),
  (
    req: { params: { id: string }; body: PenaltyCreateUpdate },
    res: Response
  ): void => {
    updatePenalty(Number(req.params.id), req.body).then(
      (_) => {
        res.json(_);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

router.delete(
  "/penalty/:id",
  auth,
  (req, res, next) => penaltyAuth(req.params.id, req, res, next),
  (req: { params: { id: string } }, res: Response): void => {
    deletePenalty(req.params.id).then(
      (_) => {
        res.json(_);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

router.delete(
  "/penalty/:eventId/event",
  auth,
  (req, res, next) => penaltiesAuth(req.body.eventId, req, res, next),
  (req: { params: { eventId: string } }, res: Response): void => {
    deleteAllPenaltyByEvent(req.params.eventId).then(
      (_) => {
        res.json(_);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

export default router;
