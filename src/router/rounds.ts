import express, { Response, Router } from "express";
import { RoundCreate } from "../model/round/round-create";
import {
  createRound,
  getRound,
  roundAuth,
  roundsAuth,
  updateRound,
} from "../service/rounds";
import { RoundUpdate } from "../model/round/round-update";
import auth from "../service/auth";

const router: Router = express.Router();

router.get(
  "/round/:eventId",
  (req: { params: { eventId: string } }, res: Response): void => {
    getRound(req.params.eventId).then(
      (roundDto) => {
        res.json(roundDto);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

router.post(
  "/rounds",
  auth,
  (req, res, next) => roundsAuth(req.body.eventId, req, res, next),
  (req: { body: RoundCreate }, res: Response): void => {
    createRound(req.body).then(
      (_) => {
        res.json(_);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

router.put(
  "/round/:id",
  auth,
  (req, res, next) => roundAuth(req.params.id, req, res, next),
  (req: { params: { id: string }; body: RoundUpdate }, res: Response): void => {
    updateRound(req.params.id, req.body).then(
      (_) => {
        res.json(_);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

export default router;
