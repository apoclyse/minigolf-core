import express, { Response, Router } from "express";
import {
  createTeam,
  deleteTeam,
  getTeams,
  teamAuth,
  teamsAuth,
  updateTeam,
} from "../service/teams";
import { TeamCreateUpdate } from "../model/team/team-create-update";
import auth from "../service/auth";

const router: Router = express.Router();

router.get(
  "/teams/:eventId",
  (req: { params: { eventId: string } }, res: Response): void => {
    getTeams(req.params.eventId).then(
      (teamDtos) => {
        res.json(teamDtos);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

router.post(
  "/teams",
  auth,
  (req, res, next) => teamsAuth(req.body.eventId, req, res, next),
  (req: { body: TeamCreateUpdate }, res: Response): void => {
    createTeam(req.body).then(
      (_) => {
        res.json(_);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

router.put(
  "/team/:id",
  auth,
  (req, res, next) => teamAuth(req.params.id, req, res, next),
  (
    req: { params: { id: string }; body: TeamCreateUpdate },
    res: Response
  ): void => {
    updateTeam(req.params.id, req.body).then(
      (_) => {
        res.json(_);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

router.delete(
  "/team/:id",
  auth,
  (req, res, next) => teamAuth(req.params.id, req, res, next),
  (req: { params: { id: string } }, res: Response): void => {
    deleteTeam(req.params.id).then(
      (_) => {
        res.json(_);
      },
      (err) => {
        res.status(500).send(err);
      }
    );
  }
);

export default router;
