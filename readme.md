issues:
- poradie timov sa urcuje na zaklade styroch kol, bug: pocas MSR (majstrovsta slovenskej republiky) sa hra osem kol, v appke je zapocitanych vsetkych osem kol pricom maju byt len styri
- pri pridavani hracov na jedlotlive turnaje chceme mat moznost editovat aj ich kategoriu, co sa momentalne neda
- prehlad turnajov obsahuje vsetky turnaje udporiadane podla datumu, pre dalsie roky to treba rozdelit do osobitnych prehladov (tabuliek) podla toho aku sezonu si chce uzivatel pozriet
- pridat moznost editovat kategoriu hraca pocas uz rozohrateho turnaja (potrebne migrovat databazu)
- pridat statistiky hracov na drahach za cely turnaj ale aj jednotlivo po rozkliknuti lubovolneho hraca
- implementovat automaticke zalohovanie databazy po kazdom odohratom turnaji


pre blizsie info nas nevahajte kontaktovat na: minigolf.vysledky@gmail.com