-- MySQL dump 10.13  Distrib 8.0.26, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: minigolf
-- ------------------------------------------------------
-- Server version	8.0.26-0ubuntu0.20.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clubs`
--

DROP TABLE IF EXISTS `clubs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clubs` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` varchar(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `clubs_id_uindex` (`id`),
  UNIQUE KEY `clubs_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clubs`
--

LOCK TABLES `clubs` WRITE;
/*!40000 ALTER TABLE `clubs` DISABLE KEYS */;
INSERT INTO `clubs` VALUES (1,'2021-09-15','GK Bankov'),(2,'2021-09-15','GK Trnava');
/*!40000 ALTER TABLE `clubs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `courses` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` varchar(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `lane_1` varchar(50) NOT NULL,
  `lane_2` varchar(50) NOT NULL,
  `lane_3` varchar(50) NOT NULL,
  `lane_4` varchar(50) NOT NULL,
  `lane_5` varchar(50) NOT NULL,
  `lane_6` varchar(50) NOT NULL,
  `lane_7` varchar(50) NOT NULL,
  `lane_8` varchar(50) NOT NULL,
  `lane_9` varchar(50) NOT NULL,
  `lane_10` varchar(50) NOT NULL,
  `lane_11` varchar(50) NOT NULL,
  `lane_12` varchar(50) NOT NULL,
  `lane_13` varchar(50) NOT NULL,
  `lane_14` varchar(50) NOT NULL,
  `lane_15` varchar(50) NOT NULL,
  `lane_16` varchar(50) NOT NULL,
  `lane_17` varchar(50) NOT NULL,
  `lane_18` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `courses_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses`
--

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cups`
--

DROP TABLE IF EXISTS `cups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cups` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` varchar(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `clubs_id_points` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cup_id_uindex` (`id`),
  UNIQUE KEY `cups_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cups`
--

LOCK TABLES `cups` WRITE;
/*!40000 ALTER TABLE `cups` DISABLE KEYS */;
INSERT INTO `cups` VALUES (1,'2021-09-15','liga 2005','1:12;2:45'),(2,'2021-09-15','liga 2006','1:10;3:4');
/*!40000 ALTER TABLE `cups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `events` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` varchar(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `players_id` varchar(10000) DEFAULT NULL,
  `rounds_id` varchar(10000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `events_id_uindex` (`id`),
  UNIQUE KEY `events_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (3,'2021-09-06','Trnava 2001','1;2','1;3'),(4,'2021-09-06','Bankov 1201',NULL,NULL),(5,'2021-09-07','Bankov 120',NULL,NULL);
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `penalty`
--

DROP TABLE IF EXISTS `penalty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `penalty` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` varchar(10) NOT NULL,
  `event_id` int NOT NULL,
  `round_id` int NOT NULL,
  `score_id` int NOT NULL,
  `player_id` int NOT NULL,
  `penalty` int NOT NULL,
  `text` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `penalty_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `penalty`
--

LOCK TABLES `penalty` WRITE;
/*!40000 ALTER TABLE `penalty` DISABLE KEYS */;
/*!40000 ALTER TABLE `penalty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `players`
--

DROP TABLE IF EXISTS `players`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `players` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` varchar(10) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `category` varchar(10) NOT NULL,
  `clubs_id` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `players_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `players`
--

LOCK TABLES `players` WRITE;
/*!40000 ALTER TABLE `players` DISABLE KEYS */;
INSERT INTO `players` VALUES (1,'2021-09-06','Jaroslav','Murín','M',NULL),(2,'2021-09-20','Peter','Murín','MS2',NULL),(3,'2021-09-20','Marianna','Murínová','M',NULL),(5,'2021-09-20','Adam','Zidan','ZS',NULL);
/*!40000 ALTER TABLE `players` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rounds`
--

DROP TABLE IF EXISTS `rounds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rounds` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` varchar(10) NOT NULL,
  `event_id` int NOT NULL,
  `course_id` int NOT NULL,
  `name` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rounds_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rounds`
--

LOCK TABLES `rounds` WRITE;
/*!40000 ALTER TABLE `rounds` DISABLE KEYS */;
INSERT INTO `rounds` VALUES (1,'08-02-2012',0,0,'test');
/*!40000 ALTER TABLE `rounds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scores`
--

DROP TABLE IF EXISTS `scores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `scores` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` varchar(10) NOT NULL,
  `event_id` int NOT NULL,
  `round_id` int NOT NULL,
  `player_id` int NOT NULL,
  `lane_1` int NOT NULL,
  `lane_2` int NOT NULL,
  `lane_3` int NOT NULL,
  `lane_4` int NOT NULL,
  `lane_5` int NOT NULL,
  `lane_6` int NOT NULL,
  `lane_7` int NOT NULL,
  `lane_8` int NOT NULL,
  `lane_9` int NOT NULL,
  `lane_10` int NOT NULL,
  `lane_11` int NOT NULL,
  `lane_12` int NOT NULL,
  `lane_13` int NOT NULL,
  `lane_14` int NOT NULL,
  `lane_15` int NOT NULL,
  `lane_16` int NOT NULL,
  `lane_17` int NOT NULL,
  `lane_18` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `scores_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scores`
--

LOCK TABLES `scores` WRITE;
/*!40000 ALTER TABLE `scores` DISABLE KEYS */;
INSERT INTO `scores` VALUES (1,'12-3-2021',0,1,1,1,2,1,7,4,1,2,2,4,1,2,4,1,1,1,2,4,1);
/*!40000 ALTER TABLE `scores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teams`
--

DROP TABLE IF EXISTS `teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `teams` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` varchar(10) NOT NULL,
  `event_id` int NOT NULL,
  `club_id` int NOT NULL,
  `player_1` int DEFAULT NULL,
  `player_2` int DEFAULT NULL,
  `player_3` int DEFAULT NULL,
  `player_4` int DEFAULT NULL,
  `player_5` int DEFAULT NULL,
  `player_6` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `teams_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teams`
--

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-21 11:34:32
