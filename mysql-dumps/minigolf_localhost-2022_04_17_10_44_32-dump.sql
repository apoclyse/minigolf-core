-- MySQL dump 10.13  Distrib 8.0.28, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: minigolf
-- ------------------------------------------------------
-- Server version	8.0.28-0ubuntu0.20.04.3

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `accounts` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `login` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ADMIN',
  `event_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `accounts_id_uindex` (`id`),
  UNIQUE KEY `accounts_login_uindex` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
INSERT INTO `accounts` VALUES (1,'2021-10-09','admin','$2a$10$/6nIyNcS5ad0Unb0i8eSCuNPkb7jFOnnFM.1aiyOG8qwh9WhsFqHG','ADMIN',NULL),(2,'2021-03-07','jaro','$2a$12$HpyLp/c4fu3YtrwX/lg5hexoxrF9auld6elTj.x1Xu437LgcTY7Rq','OWNER',NULL);
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clubs`
--

DROP TABLE IF EXISTS `clubs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clubs` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` varchar(10) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `clubs_id_uindex` (`id`),
  UNIQUE KEY `clubs_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clubs`
--

LOCK TABLES `clubs` WRITE;
/*!40000 ALTER TABLE `clubs` DISABLE KEYS */;
INSERT INTO `clubs` VALUES (10,'2021-09-25','GK Bankov Košice'),(11,'2021-09-25','GK Trnava \"A\"'),(12,'2021-09-25','KDG Bratislava'),(13,'2021-09-25','1.MTGC Prievidza'),(14,'2021-09-25','GK Trnava \"B\"'),(15,'2021-09-25','MTGK Brezno'),(16,'2021-09-25','MGC Classic Košice'),(17,'2021-09-25','Relax Park Prešov'),(18,'2021-09-25','TJ MG Cheb'),(19,'2021-09-25','Seefeld- Kadolz');
/*!40000 ALTER TABLE `clubs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `courses` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` varchar(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `lane_1` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lane_2` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lane_3` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lane_4` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lane_5` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lane_6` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lane_7` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lane_8` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lane_9` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lane_10` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lane_11` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lane_12` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lane_13` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lane_14` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lane_15` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lane_16` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lane_17` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lane_18` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `courses_id_uindex` (`id`),
  UNIQUE KEY `courses_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses`
--

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
INSERT INTO `courses` VALUES (3,'2021-09-25','Bankov','Segmenty','Trúbka','Ľadvina','Karabach','Most','V','Uhol','Okno','Blesk','Pyramídy','Loping','Priečky','Brucho','Sopka','Vlny','Kvet','Slimák','Kôš');
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cups`
--

DROP TABLE IF EXISTS `cups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cups` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` varchar(10) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clubs_id_points` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cup_id_uindex` (`id`),
  UNIQUE KEY `cups_name_uindex` (`name`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cups`
--

LOCK TABLES `cups` WRITE;
/*!40000 ALTER TABLE `cups` DISABLE KEYS */;
INSERT INTO `cups` VALUES (17,'2022-04-01','1.LIGA DRUŽSTIEV - 2021/2022','stav po 2. kole','17:0:0:0:0;14:434:16:27.125:2;16:826:32:25.813:7;11:834:32:26.063:8;15:791:32:24.719:10;13:785:32:24.531:12;10:749:32:23.406:15;12:760:32:23.75:16');
/*!40000 ALTER TABLE `cups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `events` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` varchar(10) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `organizer` varchar(100) NOT NULL,
  `dateFrom` varchar(10) NOT NULL,
  `dateTo` varchar(10) DEFAULT NULL,
  `state` varchar(20) NOT NULL DEFAULT 'UNAVAILABLE',
  PRIMARY KEY (`id`),
  UNIQUE KEY `events_id_uindex` (`id`),
  UNIQUE KEY `events_name_uindex` (`name`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (12,'2021-09-25','2. kolo 1.ligy','Košice Bankov','26.09.2021',NULL,'AVAILABLE'),(32,'2022-04-01','1. kolo 1. ligy','Trnava','05.09.2021',NULL,'UNAVAILABLE'),(33,'2022-04-01','3. kolo 1. ligy','V.Krtíš','08.05.2022',NULL,'UNAVAILABLE'),(34,'2022-04-01','4. kolo 1. ligy','Prešov','29.05.2022',NULL,'UNAVAILABLE'),(35,'2022-04-01','6. kolo 1. ligy','Dubnica','17.07.2022',NULL,'UNAVAILABLE'),(39,'2022-04-02','5. kolo 1. ligy','Prievidza','26.06.2022',NULL,'UNAVAILABLE'),(40,'2022-04-02','7. kolo 1. ligy + MSR','Brezno','29.07.2022','31.07.2022','UNAVAILABLE');
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `penalty`
--

DROP TABLE IF EXISTS `penalty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `penalty` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` varchar(10) NOT NULL,
  `event_id` int NOT NULL,
  `round_index` int NOT NULL,
  `score_id` int NOT NULL,
  `player_id` int NOT NULL,
  `penalty` int NOT NULL,
  `type` varchar(10) NOT NULL,
  `text` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `penalty_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `penalty`
--

LOCK TABLES `penalty` WRITE;
/*!40000 ALTER TABLE `penalty` DISABLE KEYS */;
/*!40000 ALTER TABLE `penalty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `players`
--

DROP TABLE IF EXISTS `players`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `players` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` varchar(10) NOT NULL,
  `firstname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(10) NOT NULL,
  `club_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `players_id_uindex` (`id`),
  UNIQUE KEY `players_pk` (`firstname`,`lastname`,`category`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `players`
--

LOCK TABLES `players` WRITE;
/*!40000 ALTER TABLE `players` DISABLE KEYS */;
INSERT INTO `players` VALUES (21,'2021-09-25','Jaroslav','Murín','M',10),(22,'2021-09-25','Peter','Murín','MS2',10),(23,'2021-09-25','Marianna','Drabčíková','Z',10),(24,'2021-09-25','Pavol','Murín','M',10),(25,'2021-09-25','Michal','Drabčík','M',10),(26,'2021-09-25','Pavol','Vrchovinský','MS2',10),(27,'2021-09-25','Lukáš','Barylík','M',10),(28,'2021-09-25','Ján','Slivka','MS2',16),(29,'2021-09-25','Janka','Slivková','ZS',16),(30,'2021-09-25','Julián','Klein','MS1',16),(31,'2021-09-25','Rastislav','Kováč','M',16),(32,'2021-09-25','René','Šimanský','MS1',11),(33,'2021-09-25','Štefan','Eiben','MS2',10),(34,'2021-09-25','Jakub','Kalník','M',12),(35,'2021-09-25','Lubomír','Zontág','M',12),(36,'2021-09-25','Michal','Putnoky','MS2',13),(37,'2021-09-25','Ján','Paytina','M',12),(38,'2021-09-25','Milota','Čárna','ZS',16),(39,'2021-09-25','Vanda','Kupcová','ZS',NULL),(40,'2021-09-25','František','Drgon','MS2',NULL),(41,'2021-09-25','Tibor','Meszároš','MS2',NULL),(42,'2021-09-25','Michal','Pajbach','M',NULL),(43,'2021-09-25','Milan','Luspaj','M',NULL),(44,'2021-09-25','Milan','Pätoprstý','M',NULL),(45,'2021-09-25','Štefan','Buchcár','MS2',NULL),(46,'2021-09-25','Marcel','Korínek','MS1',NULL),(47,'2021-09-25','Jozef','Čárny','MS2',NULL),(48,'2021-09-25','Vladimír ','Pravotiak','MS1',NULL),(49,'2021-09-25','Adrian','Obal','M',NULL),(50,'2021-09-25','Maroš','Jágerský','MS1',NULL),(51,'2021-09-25','Katarína','Belková','ZJ',NULL),(52,'2021-09-25','Ivan','Pätoprstý','MS2',NULL),(53,'2021-09-25','Ladislav','Ištván','MS1',NULL),(54,'2021-09-25','Božena','Pätoprstá','ZS',NULL),(55,'2021-09-25','Elena','Kupcová','Z',NULL),(56,'2021-09-25','Jakub','Šesták','M',NULL),(57,'2021-09-25','Ján','Tolarovič','MS1',NULL),(58,'2021-09-25','Michal','Ondovčík','MJ',NULL),(59,'2021-09-25','Richard','Gaži','MJ',NULL),(60,'2021-09-25','Vladislav','Oleár','MS1',NULL),(61,'2021-09-25','Viktória','Dudášová','ZJ',NULL),(62,'2021-09-25','Ján','Juhos','M',NULL),(63,'2021-09-25','Igor','Qualich','MS1',NULL),(64,'2021-09-25','Jozef','Kudernáč','MS2',NULL),(65,'2021-09-25','Igor','Ondovčík','M',NULL),(66,'2021-09-25','Štefan','Trojčák','MS1',NULL),(67,'2021-09-25','Ivan','Tkáč','MS2',NULL),(68,'2021-09-25','Angelika','Trojčáková','ZS',NULL),(69,'2021-09-25','Marek','Belko','M',NULL);
/*!40000 ALTER TABLE `players` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rounds`
--

DROP TABLE IF EXISTS `rounds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rounds` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` varchar(10) NOT NULL,
  `event_id` int NOT NULL,
  `rounds` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rounds_id_uindex` (`id`),
  UNIQUE KEY `rounds_event_id_uindex` (`event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rounds`
--

LOCK TABLES `rounds` WRITE;
/*!40000 ALTER TABLE `rounds` DISABLE KEYS */;
INSERT INTO `rounds` VALUES (16,'2021-09-25',12,'[{\"courseId\":3,\"name\":\"E1\"},{\"courseId\":3,\"name\":\"E2\"},{\"courseId\":3,\"name\":\"E3\"},{\"courseId\":3,\"name\":\"E4\"}]'),(17,'2022-04-01',32,'[]'),(18,'2022-04-01',33,'[]'),(19,'2022-04-01',34,'[]'),(20,'2022-04-01',35,'[]'),(21,'2022-04-02',39,'[]'),(22,'2022-04-02',40,'[]');
/*!40000 ALTER TABLE `rounds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scores`
--

DROP TABLE IF EXISTS `scores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `scores` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` varchar(10) NOT NULL,
  `event_id` int NOT NULL,
  `player_id` int NOT NULL,
  `club_id` int NOT NULL,
  `score` varchar(1000) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `scores_id_uindex` (`id`),
  UNIQUE KEY `scores_pk` (`event_id`,`player_id`)
) ENGINE=InnoDB AUTO_INCREMENT=502 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scores`
--

LOCK TABLES `scores` WRITE;
/*!40000 ALTER TABLE `scores` DISABLE KEYS */;
INSERT INTO `scores` VALUES (142,'2021-09-25',12,33,10,'132121212122111123;242112121211111221;111111211111111111;121211221132111111'),(146,'2021-09-25',12,36,13,'111221111112211121;112311112111212111;211111221121111121;212111221111111111'),(150,'2021-09-25',12,37,12,'211111212112112131;111211213122112111;211111111112212111;231111211122112111'),(154,'2021-09-25',12,38,16,'212211111112111131;352321224311712162;113311211123312111;133121212123111232'),(158,'2021-09-25',12,39,15,'331221212122111111;111211212122112152;121221211112412111;121111212312111111'),(162,'2021-09-25',12,40,11,'155221111112452121;213121253113112141;132121211111322111;212111113121211111'),(166,'2021-09-25',12,23,10,'111221111121112111;411211211211111121;213321111211111112;212721111113312111'),(170,'2021-09-25',12,41,13,'113211121122112111;121121111122111121;111321213112111211;312521211121111111'),(174,'2021-09-25',12,42,12,'123111113222111112;141322211123111113;131421222222112122;121111141323211113'),(178,'2021-09-25',12,28,16,'162121212211311121;111221211111112213;116122111312111121;213111131111111141'),(182,'2021-09-25',12,43,15,'151121112111111111;221321112111211111;221111122121111221;121121112122111113'),(186,'2021-09-25',12,44,11,'232211221122211131;123121122112112111;131211111121111121;111111122421111111'),(190,'2021-09-25',12,22,10,'112111122211312122;221121111311211111;112211211121111111;112211111112111111'),(194,'2021-09-25',12,45,13,'311121352122112131;221121231122411121;112111211221121111;111111111212111121'),(198,'2021-09-25',12,46,12,'113111221121111111;111221122122111111;121411112121111111;121211111111211111'),(202,'2021-09-25',12,47,16,'211221221122412111;121211211111411121;322211211122612121;111111212112111111'),(206,'2021-09-25',12,48,15,'112221111121111111;131121111121111121;111111223111111111;111111211111111121'),(210,'2021-09-25',12,49,11,'321111112112121111;132121112121711121;111111211112411211;211121212111111111'),(214,'2021-09-25',12,24,10,'111411111111111121;121121112111112111;111211111112111111;222221111111111121'),(218,'2021-09-25',12,50,13,'111111121111311111;112121211223111111;121111112121111111;131111131112211111'),(222,'2021-09-25',12,34,12,'112121212121211111;311211221122112111;121221121121111111;231111131112111131'),(226,'2021-09-25',12,29,16,'141411121111512141;221521221112112212;112121222112111111;233111111312211121'),(230,'2021-09-25',12,51,15,'111221112121111111;121221111323112111;121721112125111111;111111111111511111'),(234,'2021-09-25',12,52,11,'136112132122111111;233221112122111111;111111121111111411;126412112112511111'),(238,'2021-09-25',12,27,10,'122211111111111111;311111211121111121;211111221111111111;211211222122111111'),(242,'2021-09-25',12,35,12,'111111112221112111;111121113121111111;111211211111112111;111211111121111121'),(258,'2021-09-25',12,30,16,'121311111112211111;122221112121111111;152521211121111111;112122121111111111'),(262,'2021-09-25',12,53,15,'123221122123211131;143111122111411211;111211112212111121;112121112121411121'),(266,'2021-09-25',12,54,11,'234221141112512111;111121222111711121;431111121121411141;141211121121212221'),(270,'2021-09-25',12,21,10,'111121111121111121;111121111111111121;114121111111312111;111111111111111121'),(274,'2021-09-25',12,31,16,'111111111111112121;111121221112111141;111211212122511121;111122111221112111'),(278,'2021-09-25',12,55,15,'123321212121312111;121521123111111111;111321121121111121;111211211221211111'),(282,'2021-09-25',12,32,11,'121111121111211121;121111211122111111;111111211112111111;123311122111312121'),(286,'2021-09-25',12,56,10,'152121121222122112;111121131111112121;126111212312112224;341121122112412124'),(290,'2021-09-25',12,25,10,'111221121322611121;112411122111311111;121121221112112121;112121211222412112'),(294,'2021-09-25',12,57,18,'141111111121111111;111212111221121111;211121112121111111;131111111112411111'),(298,'2021-09-25',12,58,16,'231712112422122212;171222242322112137;224421213222432141;224115113123412113'),(302,'2021-09-25',12,59,16,'226423212242222122;123121221112122136;111121521122112123;223121244131312211'),(306,'2021-09-25',12,26,10,'112111231111112111;427211141112112112;341211131211211121;233221122121312112'),(310,'2021-09-25',12,60,16,'161121251112112114;114121122121212111;332111213111111111;151111111113221233'),(314,'2021-09-25',12,61,16,'315221212113312121;223313122112412222;272122222222411112;141121211113711211'),(318,'2021-09-25',12,62,12,'231321121122311231;231121211112211114;111221111111112121;123112211321111321'),(322,'2021-09-25',12,63,19,'111111341221111111;112121111112511111;121111111112121121;112111212111211111'),(326,'2021-09-25',12,64,10,'152521111121311121;331111221112211121;121121242122712231;123112121212112121'),(330,'2021-09-25',12,65,16,'233421124411312331;113112234322112151;242311123232122122;323112122411112114'),(334,'2021-09-25',12,66,10,'131111111221112121;152221111113112111;121121211122112121;111411121122412111'),(338,'2021-09-25',12,67,10,'242511213111112112;111222113123112112;172123121122112122;151221232123112111'),(342,'2021-09-25',12,68,10,'463111122122312131;133311122413112111;313511242122222113;171111211121422111'),(346,'2021-09-25',12,69,15,'221221222124712111;243422422131213221;231124212123222122;351121223113212111');
/*!40000 ALTER TABLE `scores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `snapshots`
--

DROP TABLE IF EXISTS `snapshots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `snapshots` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_id` int NOT NULL,
  `snapshot` varchar(10000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `snapshots_event_id_uindex` (`event_id`),
  UNIQUE KEY `snapshots_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `snapshots`
--

LOCK TABLES `snapshots` WRITE;
/*!40000 ALTER TABLE `snapshots` DISABLE KEYS */;
/*!40000 ALTER TABLE `snapshots` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teams`
--

DROP TABLE IF EXISTS `teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `teams` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` varchar(10) NOT NULL,
  `event_id` int NOT NULL,
  `club_id` int NOT NULL,
  `player_1` int DEFAULT NULL,
  `player_2` int DEFAULT NULL,
  `player_3` int DEFAULT NULL,
  `player_4` int DEFAULT NULL,
  `player_5` int DEFAULT NULL,
  `player_6` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `teams_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teams`
--

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
INSERT INTO `teams` VALUES (11,'2021-09-25',12,10,33,23,22,24,27,21),(12,'2021-09-25',12,13,36,41,45,50,NULL,NULL),(13,'2021-09-25',12,12,37,42,46,34,35,NULL),(14,'2021-09-25',12,16,38,28,47,29,30,31),(15,'2021-09-25',12,15,39,43,48,51,53,55),(16,'2021-09-25',12,11,40,44,49,52,54,NULL);
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-17 10:44:32
